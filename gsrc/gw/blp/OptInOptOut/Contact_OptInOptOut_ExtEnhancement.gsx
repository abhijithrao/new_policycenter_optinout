package gw.blp.OptInOptOut

uses gw.pl.logging.LoggerCategory
uses java.lang.Exception

/**
 * This is an enhancement class to Contact entity which handles the OptInOptOut functionality.
 * @author Rajendra Kumar
 *
 */
enhancement Contact_OptInOptOut_ExtEnhancement: entity.Contact {
  /**
   * Sets the default value for the 'Opt','ContactPreferenceType' and 'Last Source of Update' fields of digital, phone or paper mail contact preferences.
   */
 /* function setDefaultCommercialContactPreferenceOptions() {
    for (type in typekey.OptInOptOutPreferType_Ext.getTypeKeys(false)) {
      var contactPreferenceOption = new OptInOptOutPreference_Ext()
      switch (type.DisplayName) {

        case "Digital":
            contactPreferenceOption.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.OptInOptOutPreferType_Ext.TC_DIGITAL
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break

        case "Calls":
            contactPreferenceOption.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.OptInOptOutPreferType_Ext.TC_CALLS
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break

        case "Mails":
            contactPreferenceOption.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
            contactPreferenceOption.ContactPreferenceType = typekey.OptInOptOutPreferType_Ext.TC_MAILS
            this.addToContactPreferenceOptions_Ext(contactPreferenceOption)
            break
      }
    }
  }

  *//**
   * Checks if the contact has any existing contact preference options. If yes, returns the same.
   * If not, returns the list of contact preferences with predefined default values.
   *//*
  @Returns("a value to indicate the method has been called")
  property get defaultCommercialContactPreferenceOptions(): List<OptInOptOutPreference_Ext> {
    if (this.ContactPreferenceOptions_Ext.Count == 0 or this.ContactPreferenceOptions_Ext == null)
    {
      setDefaultCommercialContactPreferenceOptions()

    }
    return this.ContactPreferenceOptions_Ext.toList()
  }


  *//**
   * Updates contact preference fields('Last Update Date Time' and 'Last Source of Update') of digital, phone or paper mail, whenever there is a change in their respective
   * 'opt' option.
   *//*
  @Param("contactPreferenceOption", "the particular contact preference row, where the optin optout option was changed")
  function updateContactPreferenceFields(contactPreferenceOption: OptInOptOutPreference_Ext) {
    contactPreferenceOption.isFieldChanged("Opt")
  {
    if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_UNDEFINED) {

       contactPreferenceOption.ConfirmationStatus =null
       contactPreferenceOption.LatestUpdateDateTime = ""
       contactPreferenceOption.LastSourceOfUpdate = null
    }
    else {

      *//* updates the confirmation status as 'Waiting for confirmation' if
         the newly selected value of 'Opt' is OptIn
         and  the original value of Opt is OptOut (or) original value of Opt,Last Source of update and Latest updated date time is null *//*
      if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
          and
          (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTOUT
              or (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == null
                  and contactPreferenceOption.getOriginalValue("LastSourceOfUpdate") as typekey.LastSourceOfUpdate_Ext == null and contactPreferenceOption.LatestUpdateDateTime == null))) {

        contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
      }


          *//* updates the confirmation status as 'Waiting for confirmation' if
               the newly selected value of 'Opt' is OptIn
               and  the original value of Opt is Undefined (or) the original value of Opt is null and Last Source of update and Latest updated date time is null
               *//*
      else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
          and
          ( ((contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == null
              and contactPreferenceOption.getOriginalValue("LastSourceOfUpdate") as typekey.LastSourceOfUpdate_Ext == null )) or (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext ==typekey.ContactPreferOption_Ext.TC_UNDEFINED
              and contactPreferenceOption.getOriginalValue("LastSourceOfUpdate") as typekey.LastSourceOfUpdate_Ext == null) )) {

        contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
      }

          *//* updates the confirmation status as 'Waiting for confirmation' if
             the newly selected value of 'Opt' is OptIn
             and  the original value of Opt is Undefined (or) the original value of Opt is optout and Last Source of update is internal
           *//*
      else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
            and
            (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTOUT
                and contactPreferenceOption.getOriginalValue("LastSourceOfUpdate") as typekey.LastSourceOfUpdate_Ext == typekey.LastSourceOfUpdate_Ext.TC_INTERNAL )) {

          contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
        }


            *//* updates the confirmation status as 'Confirmed' if
               newly selected value of Opt is OptOut and
               original value of opt is OptIn*//*
        else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT and
              (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTIN)) {

            contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }

              *//* updates the confirmation status as 'Waiting For Confirmation' if
                 newly selected opt value is OptIn and original value of Opt is OptIn and the
                 original value of confirmation status is 'Waiting For Confirmation'*//*
          else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN and
                (contactPreferenceOption.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTIN) and
                (contactPreferenceOption.getOriginalValue("ConfirmationStatus") == typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION)) {

              contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
            }

            else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN) {

                contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
              }

                  *//*
                     updates confirmation status as 'Confirmed', if all other conditions don't satisfy.
                   *//*
              else
              {
                contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
              }

      *//* updates latest updates date time with current date and last source of update with 'Internal', whenever
         there 'Opt' is changed.
      *//*
      contactPreferenceOption.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
      contactPreferenceOption.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_INTERNAL

    }
  }
  }

  *//**
   * Sets the confirmation status as 'Confirmed', on the click of 'Confirm' link on the screen if the previous confirmation status is
   * 'Waiting for confirmation'.   *//*
  @Param("contactPreferenceOption", "the particular contact preference option where the confirmation status is updated")

  function confirmTheReceptionOfUserConfirmationForOptInOptOutChange(contactPreferenceOption: OptInOptOutPreference_Ext) {
    if (contactPreferenceOption.ConfirmationStatus == typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION){
      contactPreferenceOption.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
    }
  }

  *//**
   * Creates activities based on the input code received  *//*
  @Param("code", "a unique code to identify the particular contact preference")
  private function activityCreationForOptInOptOutChange(code: String) {
    var _logger = LoggerCategory.CONFIG
    var activityPattern: ActivityPattern
    //Picks up the specific activity pattern depending upon the received code(OptIn or OptOut)
    if (code == "OptIn") {
      activityPattern = ActivityPattern?.finder.getActivityPatternByCode("optIn_Validation_Ext")
    }
    else if (code == "OptOut") {
      activityPattern = ActivityPattern?.finder.getActivityPatternByCode("optOut_Intimation_Ext")
    }

    try {

      var activity: Activity

      //Creates an account level activity based with the specific activity pattern type.
      activity = activityPattern.createAccountActivity(this.Bundle, activityPattern, this?.AccountContacts?.first()?.Account, null, null, null, null, null, null, null)
      var user = User.util.CurrentUser
      var assignableGroup = User.util.CurrentUser.AllGroups?.first() as Group
      activity.assign(assignableGroup, User.util.CurrentUser)
    }
        catch (de: Exception) {
          _logger.info(de.Message + " Activity was not created. Kindly create a Manual Activity.")
        }
  }

  *//**
   * Creates activities after the specified conditions are met *//*
  function checkForActivityCreationForOptInOptOutChange() {
    //Iterates through the contact preference options
    this.ContactPreferenceOptions_Ext.each(\contactPreferenceOption ->
    {
      *//*If the the 'Opt' value of Contact preference option is changed, then logic works according to the type of
      contact preference option*//*
      if (contactPreferenceOption.ChangedFields?.hasMatch(\changedField -> changedField == "Opt"))    {
        //Checks if the original value of 'Opt' is null
        if (contactPreferenceOption.getOriginalValue("Opt") == null) {
          switch (contactPreferenceOption.ContactPreferenceType.DisplayName) {
            case "Digital":

                *//*If yes, checks, checks if the value of 'Opt' is OptOut.
                 If yes, skips out of the loop, since 'OptOut' is the default value of 'Opt' for digital and this signifies that
                 there has been no change in the value for activity creation.*//*
                if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT)  break
                    //Else creates an activity for OptIn.
                else {
                  activityCreationForOptInOptOutChange("OptIn")
                }

                break

            case "Calls":
            case "Mails":
                *//*If yes, checks, checks if the value of 'Opt' is OptOut.
          If yes, skips out of the loop, since 'OptIn' is the default value of 'Opt' for calls/mails and this signifies that
          there has been no change in the value for activity creation.*//*
                if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN)  break
                    //Else creates an activity for OptOut.
                else {
                  activityCreationForOptInOptOutChange("OptOut")
                }
                break
          }
        }
            //If the original value of 'Opt' is not null, creates activity as per the newly assigned value of 'Opt'
        else {
          //If the Opt value has been changed to OptOut, creates an activity specific to OptIn change.
          if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN) {

            activityCreationForOptInOptOutChange("OptIn")
          }
              //If the Opt value has been changed to OptOut, creates an activity specific to OptOut change.
          else if (contactPreferenceOption.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT) {
            activityCreationForOptInOptOutChange("OptOut")
          }
        }
      }
    })
  }*/
}
