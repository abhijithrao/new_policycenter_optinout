package gw.blp.OptInOptOut

uses java.lang.Exception
uses gw.pl.logging.LoggerCategory
uses pcf.api.Location

/**
 * This is a helper class to  Commercial Contact Preference PCF files.
 * @author Rajendra Kumar
 * @see BLT-47, BLT-51, BLT-60, BLT-61, BLT-63 and BLT-64
 */
class CommercialContactPreferenceHelper_Ext {
  private static final var _logger = LoggerCategory.CONFIG
  /**
   * Updates contact preference fields('Last Update Date Time' and 'Last Source of Update') of digital, phone or paper mail, whenever there is a change in their respective
   * 'opt' option.
   */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Param("code", "a unique code to identify the particular contact preference")
  static function updateContactPreferenceFields(contact: Contact, code: String) {
    switch (code) {
      case "Digital":

          contact.DigitalContactPreference_Ext.isFieldChanged("Opt") {
          contact.DigitalContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
          contact.DigitalContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_INTERNAL

        if(contact.DigitalContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_UNDEFINED) {
          contact.DigitalContactPreference_Ext.LatestUpdateDateTime = ""
          contact.DigitalContactPreference_Ext.LastSourceOfUpdate = null
          contact.DigitalContactPreference_Ext.ConfirmationStatus = null
        }
        else {
          if (contact.DigitalContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
              and ((contact.DigitalContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTOUT) or (contact.DigitalContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {

          contact.DigitalContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
          }
          else if (contact.DigitalContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT
              and ((contact.DigitalContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTIN) or (contact.DigitalContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {
           contact.DigitalContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
          else
          {
           contact.DigitalContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
        }
      }
          break

      case "Phone":
         contact.PhoneContactPreference_Ext.isFieldChanged("Opt") {
         contact.PhoneContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
         contact.PhoneContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_INTERNAL

        if(contact.PhoneContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_UNDEFINED) {
         contact.PhoneContactPreference_Ext.LatestUpdateDateTime = ""
         contact.PhoneContactPreference_Ext.LastSourceOfUpdate = null
         contact.PhoneContactPreference_Ext.ConfirmationStatus = null
        }
        else {
          if (contact.PhoneContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
              and ((contact.PhoneContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTOUT) or (contact.PhoneContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {
            contact.PhoneContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
          }
          else if (contact.PhoneContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT
              and ((contact.PhoneContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTIN) or (contact.PhoneContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {
            contact.PhoneContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }

          else
          {
            contact.PhoneContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
        }
      }
          break
      case "PaperMail":
          contact.PaperMailContactPreference_Ext.isFieldChanged("Opt") {
        contact.PaperMailContactPreference_Ext.LatestUpdateDateTime = gw.api.util.DateUtil.currentDate()
        contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_INTERNAL

        if (contact.PaperMailContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_UNDEFINED) {
          contact.PaperMailContactPreference_Ext.LatestUpdateDateTime = ""
          contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = null
          contact.PaperMailContactPreference_Ext.ConfirmationStatus = null
        }
        else {
          if (contact.PaperMailContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN
              and ((contact.PaperMailContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTOUT) or (contact.PaperMailContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {


            contact.PaperMailContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION
          }
          else if (contact.PaperMailContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT
              and ((contact.PaperMailContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_OPTIN) or (contact.PaperMailContactPreference_Ext.getOriginalValue("Opt") as typekey.ContactPreferOption_Ext == typekey.ContactPreferOption_Ext.TC_UNDEFINED))) {

            contact.PaperMailContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }

          else
          {
            contact.PaperMailContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
        }
        break
      }
    }
  }

  /**
   * Sets the default value for the 'Opt' and 'Last Source of Update' fields of digital, phone or paper mail contact preferences.
   */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Returns("a value to indicate the method has been called")
  static function setDefaultContactPreferenceValues(contact: Contact): boolean {
    if (contact != null)  {
      if (contact?.DigitalContactPreference_Ext.Opt == null) {
        contact.DigitalContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
      }
      if (contact?.PhoneContactPreference_Ext.Opt == null) {
        contact.PhoneContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
      }
      if (contact?.PaperMailContactPreference_Ext.Opt == null) {
        contact.PaperMailContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_UNDEFINED
      }

       /*if (contact?.DigitalContactPreference_Ext.LastSourceOfUpdate == null){
         contact.DigitalContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
       }

       if (contact?.PhoneContactPreference_Ext.LastSourceOfUpdate == null){
         contact.PhoneContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
       }
       if (contact?.PaperMailContactPreference_Ext.LastSourceOfUpdate == null){
         contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
       }*/
    }
    return true
  }

  /**
   * Sets the confirmation status as 'Confirmed' for  digital, phone or paper mail contact preferences.   */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Param("code", "a unique code to identify the particular contact preference")
  @Returns("a value to indicate the method has been called")
  static function confirmTheReceptionOfWrittenConfirmation(contact: Contact, code: String): boolean {
    switch (code) {
      case "Digital":
          if (contact.DigitalContactPreference_Ext.ConfirmationStatus == typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION){
            contact.DigitalContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
          break

      case "Phone":
          if (contact.PhoneContactPreference_Ext.ConfirmationStatus == typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION){
            contact.PhoneContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
          break

      case "PaperMail":
          if (contact.PaperMailContactPreference_Ext.ConfirmationStatus == typekey.ConfirmationStatus_Ext.TC_WAITINGFORCONFIRMATION){
            contact.PaperMailContactPreference_Ext.ConfirmationStatus = typekey.ConfirmationStatus_Ext.TC_CONFIRMED
          }
          break
    }
    return true
  }

  /**
   * Creates activities based on the input code received  */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  @Param("code", "a unique code to identify the particular contact preference")
  static function activityCreation(contact: Contact, code: String) {
    var activityPattern: ActivityPattern
    if (code == "OptIn") {
      activityPattern = ActivityPattern?.finder.getActivityPatternByCode("optIn_Validation_Ext")
    }
    else if (code == "OptOut") {
      activityPattern = ActivityPattern?.finder.getActivityPatternByCode("optOut_Intimation_Ext")
    }


    try {
      var activity: Activity
      activity = activityPattern.createAccountActivity(contact.Bundle, activityPattern, contact.AccountContacts.first().Account, null, null, null, null, null, null, null)
      activity.Description = activity.Description + " for :"+contact.DisplayName
      var user = User.util.CurrentUser
      var assignableGroup = User.util.CurrentUser.AllGroups?.first() as Group
      activity.assign(assignableGroup, User.util.CurrentUser)
    }
        catch (de: Exception) {
          _logger.info(de.Message + " Activity was not created. Kindly create a Manual Activity.")
        }
  }

  /**
   * Creates activities after the specified conditions are met */
  @Param("contact", "the particular contact on which the contact preference fields are updated")
  static function checkForActivityCreation(contact: Contact) {
    if (contact.DigitalContactPreference_Ext.ChangedFields?.hasMatch(\changedField -> changedField == "Opt"))  {
      if (contact.DigitalContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN){

        activityCreation(contact, "OptIn")
      }

      if (contact.DigitalContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT) {
        activityCreation(contact, "OptOut")
      }
    }

    if (contact.PhoneContactPreference_Ext.ChangedFields?.hasMatch(\changedField -> changedField == "Opt")) {

      if (contact.PhoneContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN){

        activityCreation(contact, "OptIn")
      }

      if (contact.PhoneContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT) {
        activityCreation(contact, "OptOut")
      }
    }

    if (contact.PaperMailContactPreference_Ext.ChangedFields?.hasMatch(\changedField -> changedField == "Opt")) {

      if (contact.PaperMailContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTIN){

        activityCreation(contact, "OptIn")
      }

      if (contact.PaperMailContactPreference_Ext.Opt == typekey.ContactPreferOption_Ext.TC_OPTOUT) {
        activityCreation(contact, "OptOut")
      }
    }
  }

  /**
   * Updates default values for contact preference fields('Opt' and 'Last Source of Update') of digital, phone or paper mail, whenever a new accoutn is created.
   */
  @Param("account", "the particular account on which the contact preference fields are updated")
  static function setDefaultContactPreferenceValuesForAccount(account: Account): boolean {
    account?.AccountContacts?.each(\accountContact ->
    {

      if (accountContact.Contact.DigitalContactPreference_Ext.Opt == null) {
        accountContact.Contact.DigitalContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_OPTOUT
      }
      if (accountContact.Contact.PhoneContactPreference_Ext.Opt == null) {
        accountContact.Contact.PhoneContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_OPTIN
      }
      if (accountContact.Contact.PaperMailContactPreference_Ext.Opt == null) {
        accountContact.Contact.PaperMailContactPreference_Ext.Opt = typekey.ContactPreferOption_Ext.TC_OPTIN
      }

      if (accountContact.Contact.DigitalContactPreference_Ext.LastSourceOfUpdate == null){
        accountContact.Contact.DigitalContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
      }

      if (accountContact.Contact.PhoneContactPreference_Ext.LastSourceOfUpdate == null){
        accountContact.Contact.PhoneContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
      }
      if (accountContact.Contact.PaperMailContactPreference_Ext.LastSourceOfUpdate == null){
        accountContact.Contact.PaperMailContactPreference_Ext.LastSourceOfUpdate = typekey.LastSourceOfUpdate_Ext.TC_DEFAULT
      }
    })
    return true
  }

  function  checkUndefinedValues(account:Account){
    var accountContacts=account.AccountContacts
    for(i in accountContacts){
      if(i.Contact.DigitalContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        i.Contact.DigitalContactPreference_Ext.ConfirmationStatus=null
      }
      if(i.Contact.PhoneContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        i.Contact.PhoneContactPreference_Ext.ConfirmationStatus=null
      }
      if(i.Contact.PaperMailContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        i.Contact.PaperMailContactPreference_Ext.ConfirmationStatus=null
      }
    }
  }


  function  checkUndefinedValuesAccountContact(accountContact:AccountContact){
      if(accountContact.Contact.DigitalContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        accountContact.Contact.DigitalContactPreference_Ext.ConfirmationStatus=null
      }
      if(accountContact.Contact.PhoneContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        accountContact.Contact.PhoneContactPreference_Ext.ConfirmationStatus=null
      }
      if(accountContact.Contact.PaperMailContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
        accountContact.Contact.PaperMailContactPreference_Ext.ConfirmationStatus=null
      }
    }

  function  checkUndefinedValuesContact(contact:Contact){
    if(contact.DigitalContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
      contact.DigitalContactPreference_Ext.ConfirmationStatus=null
    }
    if(contact.PhoneContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
      contact.PhoneContactPreference_Ext.ConfirmationStatus=null
    }
    if(contact.PaperMailContactPreference_Ext.Opt==typekey.ContactPreferOption_Ext.TC_UNDEFINED){
      contact.PaperMailContactPreference_Ext.ConfirmationStatus=null
    }
  }

}
